const supertest = require('supertest');
const knex = require('../db/connect');
const app = require('../index')
const request = supertest(app);
const duplicate = [
    { "description": "Have to do my web task pending on 15", "id": 1, "title": "Web Task" }, { "description": "Have to do clean my room", "id": 2, "title": "Have to clean my room" }, { "description": "discord bot fetching currency data", "id": 3, "title": "Have to code my discord bot" }]


describe('CRUD todo', () => {
    beforeAll((done) => {
        console.log('befor All')
        knex.migrate.latest()
            .then(() => {
                return knex.seed.run();
            }).then(() => done());
        console.log('finished create table')
    });

    it('gets the test endpoint', async done => {
        const response = await request.get('/api/v1/todo')
        console.log(response.body)
        expect(response.body).toEqual(expect.arrayContaining(duplicate));
        expect(200)
        done();
    })


    afterAll((done) => {
        setTimeout(function () {
            knex.schema.dropTable('todo').then(() => (done))
            knex.schema.dropTable('knex_migrations').then(() => (done))
            knex.schema.dropTable('knex_migrations_lock').then(() => (done))
        }, 10000);

        app.close();

    }

    )


});


