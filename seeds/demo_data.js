const seed_data = require("../dummy");
exports.seed = function (knex, Promise) {

  return knex("todo")
    .del()
    .then(function () {
      return knex("todo").insert(seed_data);
    });
};
