const express = require("express");
const router = express.Router();
const queries = require("../db/qurey");
const todo_controller = require("../controllers/todo_controller");

router.get("/", todo_controller.get);

router.get("/:id", todo_controller.getById);

router.post("/", todo_controller.create);

router.put("/:id", todo_controller.update);

router.delete("/:id", todo_controller.delete);

module.exports = router;
