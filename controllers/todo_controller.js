const queries = require("../db/qurey");

exports.create = (req, res) => {
    queries.create(req.body).then(showData => {
        res.json(showData)
    });
}

exports.update = (req, res) => {
    queries.updateData(req.params.id, req.body).then(showData => {
        res.status(200).json(showData)
    });
}

exports.delete = (req, res) => {
    queries.deleteData(req.params.id).then(showData => {
        res.send("deleted: true");
        res.status(200).json(showData)
    });
}

exports.getById = (req, res) => {
    queries.getOne(req.params.id).then(showData => {
        res.status(200).json(showData)
    });
}

exports.get = (req, res) => {
    queries.getAll().then(showData => {
        res.json(showData)
    });
}
